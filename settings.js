var src = './src';
var build = './build';

module.exports = {
    src: src,
    build: build,
    js:{
        srcAll: src + 'js/**/*.js',
        srcVendors: src + '/js/vendors/*.js',
        srcComponents: src + '/js/components/*.js',
        srcJquery: src + '/js/vendors/jquery.min.js',
        srcVue: src + '/js/vendors/vue.js',
        srcVueMin: src + '/js/vendors/vue.min.js',
        srcParticles: src + '/js/vendors/particles.min.js',
        srcMain: src + '/js/main.js',
        destVendors: build + '/js',
        destComponents: build + '/js',
        destMain: build + '/js',
        destAll: build + '/js'
    },
    images:{
        all: src + '/img/**/*.+(jpg|jpeg|gif|png|ico|svg)',
        src: src + '/img',
        dest: build + '/img',
        icons: src + '/img/icons'
    },
    css:{
        all: src + '/css/**/*.scss',
        src: src + '/css',
        dest: build + '/css',
        outputStyle: 'expanded',
        outputStyleProd: 'compressed',
        prefixBrowsers: ['last 2 versions']
    },
    assets: {
        all: src + '/assets/**/*.*',
        dest: build + '/assets'
    },
    api: {
      all: src + '/api/**/*.json',
      dest: build + '/api'
    },
    clean:{
        dest: build
    }
};
