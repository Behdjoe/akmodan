var $ = require('gulp-load-plugins')();
var gulp = require('gulp');
var gulpif = require('gulp-if');
var sprite = require('gulp-sprite-generator');
var setting = require('./settings');
var browserSync = require('browser-sync').create();
var runSequence = require('run-sequence');
var reload = browserSync.reload;
var PRODUCTION = false;
$.totalTaskTime.init(); //initialize taskTime plugin.

//errors
var onError = (err) => {
  $.util.beep();
  console.error(err);
};

//clean task
gulp.task("clean", (done) => {
  gulp.src(setting.clean.dest)
    .pipe($.clean())
  done()
});

//scripts vendor bundle
gulp.task('scripts-vendors', (done) => {
  var vueVersion = PRODUCTION ? setting.js.srcVueMin : setting.js.srcVue;
  var sources = [
    setting.js.srcJquery,
    vueVersion,
    setting.js.srcParticles
  ];
  gulp.src(sources)
    .pipe($.plumber({ errorHandler: onError })) //plumber
    .pipe($.concat('bundle.vendors.js'))
    .pipe(gulp.dest(setting.js.destVendors))
    .pipe(reload({ stream: true }))
  done()
});

//scripts components bundle
gulp.task('scripts-components', (done) => {
  var sources = [
    setting.js.srcComponents,
    setting.js.srcMain
  ];
  gulp.src(sources)
    .pipe($.plumber({errorHandler: onError })) //plumber
    .pipe($.concat('bundle.min.js'))
    .pipe($.uglify())
    .pipe(gulp.dest(setting.js.destComponents))
    .pipe(reload({ stream: true }));
  done()
});

//scss
gulp.task('scss', (done) => {
  var outputStyle;
  //check for production flag
  if (PRODUCTION) {
    outputStyle = setting.css.outputStyleProd;
  } else {
    outputStyle = setting.css.outputStyle;
  }

  gulp.src(setting.css.all)
    .pipe($.plumber({ errorHandler: onError })) //plumber
    .pipe(gulpif(!PRODUCTION, $.sourcemaps.init())) //sourcemaps init
    .pipe($.sass({ outputStyle: outputStyle })) //do sass magic
    .pipe($.autoprefixer({ browsers: setting.css.prefixBrowsers })) //do autoprefixer jazz
    .pipe($.cssnano()) //uglify
    .pipe(gulpif(!PRODUCTION, $.sourcemaps.write())) //sourcemaps write
    .pipe(gulp.dest(setting.css.dest))
    .pipe(reload({ stream: true }))
  done()
});

gulp.task('img', (done) => {
  return gulp.src(setting.images.all)
    .pipe($.imagemin())
    .pipe(gulp.dest(setting.images.dest))
    done();
});

//copy assets to build folder
gulp.task('assets', (done) => {
    gulp.src(setting.assets.all)
    .pipe($.flatten())
    .pipe(gulp.dest(setting.assets.dest))
    done()
});

//copy api to build folder
gulp.task('api', (done) => {
    gulp.src(setting.api.all)
    .pipe(gulp.dest(setting.api.dest))
    done()
});

// Gulp task to copy HTML files to output directory
gulp.task('html', (done) => {
    gulp.src(setting.src + "/**/*.html")
    .pipe(gulp.dest(setting.build))
    .pipe(reload({stream: true}))
    done()
});


gulp.task('reload', (done) => {
  reload();
  done();
});

//server
gulp.task('server', (done) => {
  browserSync.init({
    server: {
      baseDir: setting.build,
    },
    ghostMode: false,
    open: false, // Stop the browser from automatically opening.
    minify: true //minify the BS client side script
  })
  done()
});

//watcher task
gulp.task('watch', (done) => {
  gulp.watch(setting.css.all, ['scss']);
  gulp.watch(setting.js.srcComponents, ['scripts-components']);
  gulp.watch(setting.js.srcMain, ['scripts-components']);
  gulp.watch(setting.images.all, ['img']);
  // gulp.watch(setting.assets.all, ['assets']);
  // gulp.watch(setting.api.all, ['api']);
  gulp.watch(setting.src + '/**/*.html', ['html']);
  done()
});

//production
gulp.task('build', () => {
  this.PRODUCTION = true;
  runSequence('html', 'api', 'img', 'scss', 'assets', 'scripts-vendors', 'scripts-components', () => {
    // gulp.start(['server'])
  });
});

//default
gulp.task('default', () => {
  runSequence('html', 'api', 'img', 'scss', 'assets', 'scripts-vendors', 'scripts-components', 'watch', () => {
    gulp.start(['server'])
  });
});
