# Project Akmodan #

Project Akmodan. A web application for rapid front-end development. Package contains gulp, scss, autoprefixer, browsersync and various other tools to help speed up development.

Check out the demo at https://akmodan.netlify.com

