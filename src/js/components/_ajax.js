// Vanilla ajax function. The function takes two parameters;
// first is an object with the request information, second is the call back.

// Usage:
// ajax({
//  url: '/path',
//  method: 'get', //(or post, put or delete)
//  data: {} //only for POSTs
// }, function(res) { //optional callback;
// console.log('done', res);
// });


//ajax wrapper
function getJson(path, callback) {
  ajax({ url: path, method: 'get', type: 'json', cache: true }, callback);
}


//ajax function
function ajax(req, callback) {
    var res = {},
        url = req.url || '',
        data = req.data || '',
        type = req.type || '',
        method = req.method || '',
        methods = ['get', 'post', 'put', 'delete'],
        noCache = req.cache ? '' : '?' + new Date().getTime(),
        errorMessage = 'something went wrong, please check your request.';

    if (!url || !method) { return console.error(errorMessage) }
    else if (methods.indexOf(method.toLowerCase()) < 0) { console.error(errorMessage) }
    else if (method == 'POST' && !data) { console.error(errorMessage) }

    //start the request
    var xhr = new XMLHttpRequest();
    xhr.open(method, url + noCache, true);
    xhr.responseType = type;
    xhr.send(data);

    // console.log(xhr);
    xhr.onload = function (e) {
      // console.log(e.target);
      res = e.target.response || e.target.responseText;

      if (typeof callback === 'function') {
          return callback(res);
      }
    };

    // return res;
};
