var galleryComponent = {
  config: {
    appId: 'galleryComponent',
    itemsPerPage: 9,
    activePage: 0, // default 0 index
    loading: true,
    loveFilter: false,
    nowOpen: {},
  },
  vue: {
    data: {},
    methods: {
      filterByLoves: function(index){
          this.loveFilter = !this.loveFilter;
      },
      viewItem: function(obj){
        this.nowOpen = obj;

        $('body').addClass('img-open');
      },
      closeItem: function(){
        this.nowOpen = {};

        $('body').removeClass('img-open');
      },
      loveIt: function(item) {
        item.love = !item.love;

        if (item.love) {
          item.likes ++;
        }
        else {
          item.likes --;
        }
      }
    },
    computed: {
      pagesComputed: function() {
        var sets = new Array(),
            set = new Array(),
            counter = 0,
            items = this.feed,
            itemsPerPage = this.itemsPerPage;


        //loop through items
        for(var i = 0; i < items.length; i++) {
          set.push(items[i]);

          //create sets (pages)
          if((i + 1) % itemsPerPage == 0 || (i + 1) >= items.length) {
              counter ++;
              sets[counter] = set;
              set = new Array();
          }
        }
        //sets becomes a 2D array.
        sets.splice(0, 1); // remove '0' item from array.
        // console.log(sets);
        return sets;
      },

      feedComputed: function() {
        if (this.feedLoves.length > 0 && this.loveFilter ) {
          return this.feedLoves;
        }
        else {
          return this.feed;
        }
      },

      feedLoves: function() {
        var filteredItems = this.feed.filter(function(item){
            return (item.love == true); //if item.love == true, return the item;
        });
        return filteredItems;
      }

    },
    directives: {
      focus: {
        inserted: function(el, binding) {
          if (binding.value) el.focus();
          else el.blur();
        },
        componentUpdated: function(el, binding) {
          if (binding.modifiers.lazy) {
            if (Boolean(binding.value) === Boolean(binding.oldValue)) {
              return;
            }
          }
          if (binding.value) el.focus();
          else el.blur();
        },
      }
    },
    mounted: function() {
      this.loading = false; // component fully loaded.

      // $('body').keypress(function(event){
      //   console.log(event);
      //   if ( event.which == 13 ) {
      //     console.log(event.which);
      //   }
      // });
    }
  },

  render: function(data) {
    var dataPromise = data || {},
        selector = '#' + this.config.appId,
        warningMessage = 'Data empty... proceed to render.';

    //exit if data is empty
    if (Object.keys(dataPromise).length === 0) {
      return console.warn(warningMessage);
    }

    // console.log(data);

    var collectiveData = Object.assign({}, dataPromise, this.config);

    // console.log(collectiveData);

    new Vue({
      el: selector,
      data: collectiveData,
      methods: this.vue.methods,
      computed: this.vue.computed,
      directives: this.vue.directives,
      mounted: this.vue.mounted,
    });

    // var components = this.vue.components;

  }, // render end

  init: function(data) {
    // console.log(data);
    this.render(data);
  }
};
