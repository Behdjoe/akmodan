var particles = {
    init: function(flag) {
      $(document).ready(function() {
        var canvasSelector = '#particles',
            colorContainer = $('#theme-color').css('color'),
            themeColor = rgb2hex(colorContainer) || '#000',
            animationSpeed = 0.2,
            maxGlobalParticles = 150,
            sizeVariations = 6,
            minDistance = 50;

          Particles.init({
            // normal options
            selector: canvasSelector,
            color: themeColor,
            maxParticles: maxGlobalParticles,
            sizeVariations: sizeVariations,
            minDistance: minDistance,
            speed: animationSpeed,
            connectParticles: true,

            // options for breakpoints
            responsive: [{
              breakpoint: 768, //tablet
              options: {
                maxParticles: 80,
                minDistance: 50,
              }
            },
            {
              breakpoint: 425, //mobile - m
              options: {
                maxParticles: 50,
                minDistance: 40,
                // connectParticles: false,
              }
            },
            {
              breakpoint: 320, //mobile -s
              options: {
                maxParticles: 30,
                minDistance: 40,
                // animationSpeed: 0.1,
              }
            }]

          });

          //catch when the browser windiw is inactive, then toggle the component.
        $(window).on("focus blur", function(e) {
          var prevType = $(this).data("prevType");

          // console.log(e);

            if (prevType != e.type) { //  reduce double fire issues
              switch (e.type) {
                case "focus":
                  Particles['options'].speed = animationSpeed;
                  Particles._refresh();
                  // console.log(Particles['options'].speed);
                  // Particles._animate(); //re-init the canvas.
                  break;
                case "blur":
                  Particles['options'].speed = 0.0;
                  Particles._refresh();
                  // console.log(Particles['options'].speed);
                  // $(canvasSelector).addClass(hideClass);
                  break;
              }
            }

            $(this).data("prevType", e.type);
          });

      });
    }
};
