var newsComponent = {
  config: {
    appId: 'newsComponent',
    itemsPerPage: 4,
    activePage: 0, // default 0 index
    loading: true,
    saving: false,
    searchQuery: '',
    searchOpen: false,
    activeEditItem: null,
    newPostObj: {
      title: '',
      body: '',
    },
  },
  vue: {
    methods: {
      newPost: function(){
        var obj = Object.assign({}, this.newPostObj),
            message = '';

        if (!this.newPostObj.title) return console.warn(message);

        this.feed.push(obj);

        obj = {};

        console.log('newPost()', obj);
      },

      searchToggle: function() {
        // console.log(this.searchOpen, this.searchQuery)
        if (!this.searchOpen && this.searchQuery) {
          // this.searchQuery = '';
        }

        this.searchOpen = !this.searchOpen;

      },

      setPage: function(index){
        this.activePage = index;
      },

      sort: function() {
        var items = this.feed;
        items.reverse();
      },

      star: function(item) {
        item.starred = !item.starred; //toggle
        // console.log(item.id, item.starred, item);
      },

      search: function() {
      },

      readMore: function(e) {
        var target = e.target,
          openClass = 'open';
        console.log('readmore');
      },

      editArticle: function(item) {
        // console.log(item)
        if (this.activeEditItem == item.id) {
          this.activeEditItem = null;
        } else {
          this.activeEditItem = item.id;
        }
      },

      doneEditing: function(item){
        var that = this;
        if (item.title == "") return console.warn('item empty');
        this.activeEditItem = item.id;
        this.saving = true;

        //wait a bit, then save;
        setTimeout(function(){
          console.log('item saved!');
          that.saving = false;
          that.activeEditItem = null;
        }, 500);

      },

      saveArticle: function(obj) {
        console.log('saving article...');
        // var items = this.feed;
      },

      deleteArticle: function(item) {
        if (confirm('are you sure')) {
          item.trashed = true;
        }
      }
    },
    computed: {
      pagesComputed: function() {
        var sets = new Array(),
            set = new Array(),
            counter = 0,
            items = this.feed,
            itemsPerPage = this.itemsPerPage;


        //loop through items
        for(var i = 0; i < items.length; i++) {
          set.push(items[i]);

          //create sets (pages)
          if((i + 1) % itemsPerPage == 0 || (i + 1) >= items.length) {
              counter ++;
              sets[counter] = set;
              set = new Array();
          }
        }
        //sets becomes a 2D array.
        sets.splice(0, 1); // remove '0' item from array.
        // console.log(sets);
        return sets;
      },

      feedComputed: function() {
        var items = this.pagesComputed[this.activePage],
            searchQuery = this.searchQuery,
            searchResults = this.searchComputed;

            if (searchQuery != '' && searchResults.length > 0) {
              return searchResults;
            }
            else {
              return items;
            }
      },

      searchComputed: function() {
        var items = this.feed,
            query = this.searchQuery.toLowerCase();

        return items.filter(function(item) {
          var title = item.title.toLowerCase(),
              body = item.body.toLowerCase();

              result = (title.indexOf(query) > -1) || (body.indexOf(query) > -1) || '';
              return result;
        });
      }
    },
    directives: {
      focus: {
        inserted: function(el, binding) {
          if (binding.value) el.focus();
          else el.blur();
        },
        componentUpdated: function(el, binding) {
          if (binding.modifiers.lazy) {
            if (Boolean(binding.value) === Boolean(binding.oldValue)) {
              return;
            }
          }
          if (binding.value) el.focus();
          else el.blur();
        },
      }
    },
    mounted: function() {
      this.loading = false; // component fully loaded.
    }
  },

  render: function(data) {
    var dataPromise = data || {},
        selector = '#' + this.config.appId,
        warningMessage = 'vue data empty, proceeding anyway.';

    if (!data) {
      console.warn(warningMessage, typeof data);
    }

    var collectiveData = Object.assign({}, this.config, dataPromise);

    // console.log(collectiveData)

    Vue.component('pagination', {
      template: '<li>{{ feed.length }}</li>',
      props: ['feed'],
      methods: {
      }

    });

    new Vue({
      el: selector,
      data: collectiveData,
      methods: this.vue.methods,
      computed: this.vue.computed,
      directives: this.vue.directives,
      mounted: this.vue.mounted,
    });

    // var components = this.vue.components;
    // console.log(App.$data, App.$el);

  }, // render end

  init: function(data) {
    this.render(data);
  }
};
