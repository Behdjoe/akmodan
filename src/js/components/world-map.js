//
// var worldMap = {
//
//   //set variables.
//   var ApiId = $('#OrganizationId').val(),
//       newApiUrl = '/json/customerwizardorganization/getorganizationinfoasjson/' + ApiId,
//       countryApiUrl = '/json/customerwizardorganization/getcountriesasjson',
//       completeOrganizationObject = {},
//
//       // UI Control flags for elements.
//       UIControlElements = {
//           "dataModified": false,
//           "saveButtonLabel": "Saved",
//           "addCountrySearchFilter": '',
//           "addCountryContainerOpen": false,
//           "addEntityInputOpen": false,
//           "addEntityServicesOpen": false,
//           "addEntitySaveValue": '',
//           "activeEntityContainer": '',
//           "countriesAdded": []
//       },
//
//       // methods for handling interactions in vue ///
//       methods = {
//           handleAddCountryToggle: function (e) {
//               var target = e.target,
//                   searchInput = $('.country-container-wrap .js--country-add-input'),
//                   targetContainer = $(target).next('div');
//
//               this.addCountryContainerOpen = !this.addCountryContainerOpen;
//           },
//           handleAddCountryClose: function (event) {
//               if (this.addCountryContainerOpen) {
//                   this.addCountryContainerOpen = false;
//               }
//           },
//
//           HandleAddCountrySearch: function (event) {
//               var target = event.target,
//                   value = target.value;
//               this.addCountrySearchFilter = value;
//           },
//
//           handleAddNewCountry: function (newCountry) {
//
//               var targetContainer = $('.js--country-container'),
//                   countryName = newCountry.Name,
//                   countryCode = newCountry.Code,
//                   obj = {
//                       "Id": newCountry.Id,
//                       "Name": countryName,
//                       "Code": countryCode,
//                       "Companies": []
//                   };
//
//               if (Array.isArray(this.Countries)) {
//                   this.Countries.push(obj);
//                   this.addCountryContainerOpen = false;
//                   this.addCountrySearchFilter = '';
//               }
//           },
//
//           handleAddEntity: function (entityId) {
//               //var items = this.Services.List;
//               this.activeEntityContainer = entityId;
//
//           },
//
//           handleAddEntitySave: function (countryObj, services) {
//               var val = this.addEntitySaveValue,
//                   items = services.List,
//                   errMessage = dictionary.translate('Wizard.Organization.Message.EntityNameRequired');
//
//               if (!val) {
//                   toastr.warning(errMessage);
//                   return;
//               }
//
//               var servicesClone = $.extend(true, {}, services), //create a clone using jq extend.
//                   newEntityObj = {
//                       "Id": val,
//                       "Name": val,
//                       "Services": servicesClone
//                   };
//
//               if (Array.isArray(countryObj.Companies)) {
//                   countryObj.Companies.push(newEntityObj);
//                   this.addEntitySaveValue = ''; //empty the input value
//                   this.activeEntityContainer = ''; //close the services list
//               }
//
//               //reset all service checkboxes to be false.
//               services.All = false;
//               items.forEach(function (item) {
//                   //console.log(item.Name, item.Checked);
//                   return item.Checked = false;
//               });
//
//           },
//
//           handleEditEntityServices: function (entityId) {
//               if (this.activeEntityContainer == entityId) {
//                   this.activeEntityContainer = '';
//               }
//               else {
//                   this.activeEntityContainer = entityId;
//               }
//
//           },
//
//           handleToggleServices: function (services) {
//               var select = this.selectAll,
//                   myServices = services.List || '';
//               if (!myServices) { return; }
//
//               myServices.forEach(function (item) {
//                   item.Checked = !select;
//               });
//               this.selectAll = !select;
//               services.All = this.selectAll;
//           },
//
//           handleSaveOrganization: function (e) {
//               this.saveButtonLabel = dictionary.translate("Common.Labels.Saving");
//               saveData(completeOrganizationObject);
//           },
//
//           handleCloseEntity: function (val) {
//               var errMessage = dictionary.translate('Wizard.Organization.Message.EntityNameRequired');
//               if (!val) {
//                   toastr.warning(errMessage);
//                   return;
//               }
//
//               this.activeEntityContainer = '';
//           },
//
//           handleCloseAllEntities: function (event) {
//               var target = $(event.target),
//                   worldMapList = $('.world-map__list');
//
//               if (target[0] == worldMapList[0]) {
//                   this.activeEntityContainer = '';
//               }
//               else {
//               }
//
//           }
//
//       },
//
//       //function for sending the entire world-map data-model back to the api.
//       saveData = function (obj, callback) {
//           $.ajax({
//               method: "PUT",
//               url: newApiUrl,
//               async: true,
//               data: obj
//           })
//           .done(function (res, status) {
//               var message = dictionary.translate("Common.Labels.SaveSuccessful");
//
//               toastr.success(message);
//               setDataModified(completeOrganizationObject, false);
//               completeOrganizationObject.saveButtonLabel = dictionary.translate("Common.Labels.Saved");
//
//               refreshVue(res); //force a re-fresh of the component.
//
//               //return the callback if it's present
//               if (typeof callback == 'function') {
//                   return callback();
//               }
//
//               return;
//           }.bind(completeOrganizationObject))
//
//           .fail(function (err) {
//               var message = dictionary.translate("Common.Labels.SaveFailed");
//               console.error(message, err.statusText);
//               toastr.error(message); //display a notification toast.
//               setDataModified(completeOrganizationObject, true);
//               completeOrganizationObject.saveButtonLabel = dictionary.translate("Common.Labels.SaveChanges");
//               return false;
//           }.bind(completeOrganizationObject));
//
//           return;
//       },
//
//       //fetch countries, runs on load.
//       fetchCountries = (function () {
//           //console.log('fetching countries: ');
//
//           return $.ajax({
//               method: 'GET',
//               url: countryApiUrl,
//               async: true,
//               cache: true
//           })
//               .done(function (res, xhr) {
//
//                   //combine UI control elements with the fetched datamodel.
//                   $.extend(completeOrganizationObject, { "presetCountries": res });
//                   //console.log('countries:', completeOrganizationObject);
//
//                   fetchData();
//                   return true;
//               })
//               .fail(function (err) {
//                   console.error('operation failed: ' + err);
//                   return false;
//               });
//       }()),
//
//       // fetches the data from the api
//       fetchData = function () {
//           //console.log('inside the caller');
//
//           return $.ajax({
//               method: 'GET',
//               url: newApiUrl,
//               async: true,
//               cache: true
//           })
//               .done(function (res, xhr) {
//                   //combine UI control elements with the fetched datamodel.
//                   $.extend(completeOrganizationObject, UIControlElements, res);
//
//                   //console.log(completeOrganizationObject);
//                   renderVue(completeOrganizationObject);
//                   return;
//               })
//               .fail(function (err) {
//                   console.error('operation failed: ' + err);
//                   return false;
//               });
//
//       },
//
//       // computed values for Vue.
//       computed = {
//           computedSelectAllPresetServices: function () {
//               var services = this.Services.List;
//
//               return services.every(function (item) {
//                   return item.Checked;
//               });
//
//           },
//
//           computedAddCountrySearchResults: function () {
//               var presetCountries = this.presetCountries.Countries,
//                   searchFilter = this.addCountrySearchFilter;
//                   addedCountries = this.computedAddedCountries;
//
//               //console.log(addedCountries, presetCountries, filteredCountryList)
//
//               return presetCountries.filter(function (item) {
//                   var itemLabel = item.Name.toLowerCase(),
//                       itemCode = item.Id.toLowerCase();
//
//                   if (addedCountries.indexOf(item.Id) > -1) {
//                       console.log(item.Name);
//                       item.added = true;
//                   }
//
//                   result = (itemCode.indexOf(searchFilter.toLowerCase()) > -1) || (itemLabel.indexOf(searchFilter.toLowerCase()) > -1) || '';
//
//                   return result;
//               });
//           },
//
//           computedAddedCountries: function () {
//               var arr = [],
//                   countries = this.Countries;
//
//               countries.forEach(function (country) {
//                   arr.push(country.Id);
//               });
//               return this.countriesAdded = arr;
//           },
//       },
//
//
//       // function for setting a (data) value from outside vue.
//       setDataModified = function (obj, val) {
//
//           if (obj === "" || val === '') {
//               console.error('parameters empty.');
//               return false;
//           } else {
//
//               if (obj.dataModified !== '') {
//
//                   obj.dataModified = val;
//                   window.dataModified = val;
//                   //console.log('dataModifiet set to: ', obj.dataModified);
//                   return;
//               }
//               else {
//                   console.error('Failed setting setDataModified flag...');
//                   return false;
//               }
//
//           }
//       },
//
//       //initializes the Vue instance and inserts the methods, computed and data values.
//       renderVue = function (data) {
//
//           //the Vue instance.
//           var worldMapApp = new Vue({
//               el: worldMapSelector,
//               data: data,
//               methods: methods,
//               computed: computed,
//               directives: directives
//           });
//
//           if (!this.globalComponent) {
//               this.globalComponent = worldMapApp; //set a reference to be used later.
//           }
//
//           if (!worldMapApp.setDataModified) {
//               //wait a bit then start the watcher.
//               setTimeout(function () {
//                   //deep watch data model for changes, set flag if detected.
//                   worldMapApp.$watch('Countries', function (newVal, oldVal) {
//                       setDataModified(worldMapApp, true);
//                       worldMapApp.saveButtonLabel = dictionary.translate("Common.Labels.SaveChanges");
//                   }, { deep: true });
//
//               }, 1000);
//           }
//
//       },
//
//       refreshVue = function (res) {
//           var component = this.globalComponent;
//           console.log('response: ', res);
//
//           //update the vue using the success response data.
//           component.Countries = res.Countries;
//
//           //wait a second, then set dataModified to false.
//           setTimeout(function () {
//               if (component.dataModified) {
//                   setDataModified(component, false);
//                   component.saveButtonLabel = dictionary.translate("Common.Labels.Saved");
//               }
//           }, 0);
//
//      },
//
//       directives = {
//           'click-outside': {
//               bind: function bind(el, binding, vNode) {
//                   // Provided expression must evaluate to a function.
//                   if (typeof binding.value !== 'function') {
//                       var compName = vNode.context.name;
//                       var warn = "[Vue-click-outside:] provided expression '${binding.expression}' is not a function, but has to be";
//                       if (compName) {
//                           warn += "Found in component '${compName}'";
//                       }
//
//                       console.warn(warn);
//                   }
//                   // Define Handler and cache it on the element
//                   var bubble = binding.modifiers.bubble;
//                   var handler = function handler(e) {
//                       if (bubble || !el.contains(e.target) && el !== e.target) {
//                           binding.value(e);
//                       }
//                   };
//                   el.__vueClickOutside__ = handler;
//
//                   // add Event Listeners
//                   document.addEventListener('click', handler);
//               },
//
//               unbind: function unbind(el, binding) {
//                   // Remove Event Listeners
//                   document.removeEventListener('click', el.__vueClickOutside__);
//                   el.__vueClickOutside__ = null;
//               }
//           },
//
//           focus: {
//               inserted: function (el, binding) {
//                   if (binding.value) el.focus();
//                   else el.blur();
//               },
//
//               componentUpdated: function (el, binding) {
//                   if (binding.modifiers.lazy) {
//                       if (Boolean(binding.value) === Boolean(binding.oldValue)) {
//                           return;
//                       }
//                   }
//
//                   if (binding.value) el.focus();
//                   else el.blur();
//               },
//           }
//       },
//
//       onbeforeunload = (function () {
//
//           window.onbeforeunload = function (e) {
//               e = e || window.event;
//               var message = dictionary.translate("Common.Labels.ChangesWillBeLost");
//
//               if (!window.dataModified) { return }
//
//               // For IE and Firefox prior to version 4
//               if (e) {
//                   e.returnValue = message;
//               }
//
//               // For Safari
//               return message;
//           };
//       }());
//
//   });
// };
