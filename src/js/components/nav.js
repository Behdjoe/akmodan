$(document).ready(function() {
  var activeClass = 'active',
      navEl = $('.js--nav li'),
      contentContainer = $('.tabber .tabber__tab'),
      hash = window.location.hash || '';

  if (hash){
    var pageName = hash.replace('#', ''),
        contentName = pageName + "-content",
        targetRadio = contentContainer.find('[id='+contentName+']'),
        targetNavItem = navEl.find('a label[for='+contentName+']');


        targetNavItem.parent().parent().addClass(activeClass);
        targetRadio.prop('checked', true);

    // console.log(targetNavItem);
  }
  else {
      var targetRadio = contentContainer.find('input').slice(0,1),
          targetNavItem = navEl.slice(0,1);

      targetRadio.prop('checked', true);
      targetNavItem.addClass(activeClass);
      // console.log('no hash, selecting first page.');
  }

  navEl.on('click', function(e){
    var pageName = e.target.parentNode.hash;
        url = '/' + pageName;
    navEl.removeClass(activeClass);
    $(this).addClass(activeClass);

    // console.log(url, document.location.hash);

    window.history.pushState(
      {
       "type" : "AK",
       'html': '',
       'pageTitle': ''
      }, '', url);

    });
});
