var todoComponent = {
  config: {
    _appId: 'todoComponent',
    // apiUrl: '/api/newsfeed.json',
    _itemsPerPage: 10,
    activePage: 0, // default 0 index
    loading: true,
    searchQuery: '',
    searchOpen: false,
    showDoneTasks: false,
    feed: [{
      _id: '',
      name: 'Take out trash',
      date: '2015-12-03T22:54:52.474Z',
      done: false,
      starred: false,
    },
    {
      _id: '',
      name: 'Do groceries',
      date: '2016-12-03T22:54:52.474Z',
      done: false,
      starred: false,
    },
    {
      _id: '',
      name: 'do taxes',
      date: '2017-12-03T22:54:52.474Z',
      done: false,
      starred: false,
    }],
    newTodoItem: {
      _id: '',
      name: '',
      date: new Date,
      done: false,
      starred: false,
    }
  },
  vue: {
    methods: {
      newTodo: function() {
        if (!this.newTodoItem.name) return;
        var newItem = Object.assign({}, this.newTodoItem);

          newItem._id = this.feed.length + 1;
          newItem.date = new Date();

        // console.log(newItem, this.newTodoItem);

        this.feed.push(newItem);
        this.newTodoItem.name = '';
      },

      deleteItem: function(id) {
        this.feed.splice(id, 1);
      },
      toggleDone: function(){
        this.showDoneTasks = !this.showDoneTasks;
      }
    },

    computed: {
      pagesComputed: function() {
      },

      feedComputed: function() {
        return this.feed;
      },

      searchComputed: function() {
        var items = this.feed,
            query = this.searchQuery.toLowerCase();

        return items.filter(function(item) {
          var title = item.title.toLowerCase(),
              body = item.body.toLowerCase();

              result = (title.indexOf(query) > -1) || (body.indexOf(query) > -1) || '';
              return result;
        });
      },

      feedTodo: function() {
        // sort by Date
        // items.sort(function(a,b) {
        //   return new Date(b.date).getTime() - new Date(a.date).getTime()
        // });
        return this.feed.filter(function(item){
          return (item.done == false); //if item.love == true, return the item;
        });
      },

      feedDone: function() {
        // filter for done
        return this.feed.filter(function(item){
            return (item.done == true); //if item.love == true, return the item;
        });
      }
    },
    directives: {
      focus: {
        inserted: function(el, binding) {
          if (binding.value) el.focus();
          else el.blur();
        },
        componentUpdated: function(el, binding) {
          if (binding.modifiers.lazy) {
            if (Boolean(binding.value) === Boolean(binding.oldValue)) {
              return;
            }
          }
          if (binding.value) el.focus();
          else el.blur();
        },
      }
    },
    mounted: function() {
      this.loading = false; // component fully loaded.
    }
  },

  render: function(data) {
    var volatileData = data || {},
        selector = '#' + this.config._appId,
        warningMessage = 'vue data empty, proceeding without it.';

    var collectiveData = Object.assign({}, this.config, volatileData);

    new Vue({
      el: selector,
      data: collectiveData,
      methods: this.vue.methods,
      computed: this.vue.computed,
      directives: this.vue.directives,
      mounted: this.vue.mounted,
    });

    // var components = this.vue.components;
    // console.log(App.$data, App.$el);

  }, // render end

  init: function(data) {
    this.render(data);
  }
};
