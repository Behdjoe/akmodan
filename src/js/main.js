//entry point main.js
$(document).ready(function() {
  // var themeColor = rgb2hex($('#theme-color').css('color'));

  // components //
  todoComponent.init();
  particles.init();

  //get data for gallery then render.
  getJson('/api/gallery.json', function(res){
    galleryComponent.init(res);
  });

  //get data for news then render.
  getJson('/api/newsfeed.json', function(res){
    newsComponent.init(res);
  });


// add code to set themeColor for android and ios
});
